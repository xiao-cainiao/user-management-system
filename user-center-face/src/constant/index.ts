/*
登录页logo地址
 */
export const SYSTEM_LOGO = "https://636f-codenav-8grj8px727565176-1256524210.tcb.qcloud.la/img/logo.png";

/*
鱼皮知识星球文档地址
 */
export const PLANET_LINK = "https://docs.qq.com/doc/DUG93dVNHbVZjZXpo";

/*
甘肃农业大学官网地址
 */
export const GSAU_LINK = "https://www.gsau.edu.cn/";

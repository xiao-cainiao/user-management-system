import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';




const Footer: React.FC = () => {
  const defaultMessage = '甘农出品';
  const currentYear = new Date().getFullYear();
  return (
    <DefaultFooter
      copyright={`${currentYear} ${defaultMessage}`}
      links={[
        {
          key: 'Gansu Agricultural University ',
          title: '甘肃农业大学',
          href: 'https://www.gsau.edu.cn/',
          blankTarget: true,
        },
        {
          key: 'github',
          title: <><GithubOutlined />AJhonD</>,
          href: 'https://github.com/AJhonD',
          blankTarget: true,
        },
        {
          key: '信息科学技术学院',
          title: '信息科学技术学院',
          href: 'https://xxkx.gsau.edu.cn/',
          blankTarget: true,
        },


      ]}
    />
  );
};

export default Footer;

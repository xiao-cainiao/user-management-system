package com.example.demo.service;

import com.example.demo.model.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;

/**
 * 用户服务
 *
 * @author AJhonD
 * @description 针对表【user】的数据库操作Service
 * @createDate 2022-03-30 21:07:21
 */
public interface UserService extends IService<User> {



    /**
     * 用户注册
     *
     * @param userAccount   用户账户
     * @param userPassword  用户密码
     * @param checkPassword 校验密码
     * @param studentCode 学号
     * @return 新用户ID
     */
    long userRegister(String userAccount, String userPassword, String checkPassword,String studentCode) throws NoSuchAlgorithmException;

    /**
     * 用户登录
     * @param userAccount 用户账户
     * @param userPassword 用户密码
     * @param request
     * @return 脱敏后的用户信息
     */
    User userLogin (String userAccount, String userPassword,HttpServletRequest request);

    /**
     * 用户脱敏
     *
     * @param originUser
     * @return
     */
    User getSafetyUser(User originUser);

    /**
     *  请求用户注销
     * @param request
     * @return
     */
    int userLogout(HttpServletRequest request);



}

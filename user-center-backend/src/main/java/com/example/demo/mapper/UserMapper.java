package com.example.demo.mapper;

import com.example.demo.model.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 32078
* @description 针对表【user】的数据库操作Mapper
* @createDate 2022-03-30 21:07:21
* @Entity com.example.demo.model.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

}





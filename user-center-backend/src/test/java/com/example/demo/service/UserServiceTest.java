package com.example.demo.service;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.example.demo.model.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * 用户服务测试
 */
@SpringBootTest
public class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    public void testAddUser(){
        User user = new User();

        user.setUsername("testAjhond");
        user.setUserAccount("123");
        user.setAvatarUrl("https://636f-codenav-8grj8px727565176-1256524210.tcb.qcloud.la/img/1621512846240-github.png");
        user.setGender(0);
        user.setUserPassword("123");
        user.setPhone("123");
        user.setEmail("123");

        boolean result = userService.save(user);
        System.out.println(user.getId());

        Assertions.assertTrue(result);

    }

    @Test
    void userRegister() throws NoSuchAlgorithmException {
        String userAccount = "yupi";
        String userPassword = "";
        String checkPassword = "12345678";
        String studentCode = "1";




        long result = userService.userRegister(userAccount, userPassword, checkPassword,studentCode);
        Assertions.assertEquals(-1,result);
        userAccount = "yu";
        result = userService.userRegister(userAccount,userPassword,checkPassword,studentCode);
        Assertions.assertEquals(-1,result);
        userAccount = "123456";
        result = userService.userRegister(userAccount,userPassword,checkPassword,studentCode);
        Assertions.assertEquals(-1,result);
        userAccount = "yu pi";
        userPassword = "12345678";
        result = userService.userRegister(userAccount,userPassword,checkPassword,studentCode);
        Assertions.assertEquals(-1,result);
        checkPassword = "123456789";
        result = userService.userRegister(userAccount,userPassword,checkPassword,studentCode);
        Assertions.assertEquals(-1,result);
        userAccount = "123";
        checkPassword = "12345678";
        result = userService.userRegister(userAccount,userPassword,checkPassword,studentCode);
        Assertions.assertEquals(-1,result);
        userAccount = "AJhonD";
        result = userService.userRegister(userAccount,userPassword,checkPassword,studentCode);
        Assertions.assertTrue(result > 0);

    }
}